﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using WebDriverBeadando_L91C6E.Pages.Widgets;

namespace WebDriverBeadando_L91C6E.Pages
{
    public class FormPage : BasePage
    {
        public FormPage(IWebDriver webDriver)
            : base(webDriver)
        {
            //PageFactory.InitElements(Driver, this);
        }

        public void Navigate()
        {
            string url = "https://www.surveymonkey.com/r/FNRVK86";
            Driver.Navigate().GoToUrl(url);
        }

        public FormPartOneWidget GetFormWidgetPartOne()
        {
            return new FormPartOneWidget(Driver);
        }

        public FormPartTwoWidget GetFormWidgetPartTwo()
        {
            return new FormPartTwoWidget(Driver);
        }

        public ResultWidget GetResultWidget()
        {
            return new ResultWidget(Driver);
        }
    }
}
