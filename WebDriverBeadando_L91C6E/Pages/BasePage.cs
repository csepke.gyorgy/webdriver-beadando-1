﻿namespace WebDriverBeadando_L91C6E.Pages
{
    using OpenQA.Selenium;

    public class BasePage
    {
        protected IWebDriver Driver;

        public BasePage(IWebDriver webDriver)
        {
            Driver = webDriver;
        }
    }
}
