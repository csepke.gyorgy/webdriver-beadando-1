﻿namespace WebDriverBeadando_L91C6E.Pages.Widgets
{
    using OpenQA.Selenium;
    using OpenQA.Selenium.Support.PageObjects;
    using System.Collections.ObjectModel;

    public class FormPartTwoWidget : BasePage
    {
        public FormPartTwoWidget(IWebDriver webDriver)
            : base(webDriver)
        {
            RadioButtons = Driver.FindElements(By.ClassName("radio-button-container"));
            TextField = Driver.FindElement(By.Id("373480136"));
            PrevButton = Driver.FindElements(By.TagName("button"))[0];
            SubmitButton = Driver.FindElements(By.TagName("button"))[1];
        }

        #region IWebElements
        //[FindsBy(How = How.ClassName, Using = "radio-button-container")]
        public ReadOnlyCollection<IWebElement> RadioButtons { get; set; }

        //[FindsBy(How = How.Id, Using = "373480136")]
        public IWebElement TextField { get; set; }

        //[FindsBy(How = How.TagName, Using = "button")]
        public IWebElement SubmitButton { get; set; }

        //[FindsBy(How = How.TagName, Using = "button")]
        public IWebElement PrevButton { get; set; }
        #endregion

        public void SelectInterestDropdownChoice(int option)
        {
            RadioButtons[option - 1].Click();
        }

        public void FillTextField(string value)
        {
            TextField.SendKeys(value);
        }

        public void ClickSubmitButton()
        {
            SubmitButton.Click();
        }

        public void ClickPrevButton()
        {
            PrevButton.Click();
        }
    }
}
