﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.ObjectModel;

namespace WebDriverBeadando_L91C6E.Pages.Widgets
{
    public class FormPartOneWidget : BasePage
    {
        public FormPartOneWidget(IWebDriver webDriver)
            : base(webDriver)
        {
            RadioButtons = Driver.FindElements(By.ClassName("radio-button-container"));
            InterestDropdown = Driver.FindElement(By.Id("373480133"));
            WebDriverBasicsDropdown = Driver.FindElement(By.Id("373480134_2479050997"));
            WebDriverAdvancedDropdown = Driver.FindElement(By.Id("373480134_2479050988"));
            PageObjectDropdown = Driver.FindElement(By.Id("373480134_2479050998"));
            DataDrivenDropdown = Driver.FindElement(By.Id("373480134_2479050999"));
            SpecflowDropdown = Driver.FindElement(By.Id("373480134_2479051000"));
            NextPageButton = Driver.FindElement(By.TagName("button"));
        }

        #region IWebElements
        //[FindsBy(How = How.ClassName, Using = "radio-button-container")]
        public ReadOnlyCollection<IWebElement> RadioButtons { get; set; }

        //[FindsBy(How = How.Id, Using = "373480133")]
        public IWebElement InterestDropdown { get; set; }

        //[FindsBy(How = How.Id, Using = "373480134_2479050997")]
        public IWebElement WebDriverBasicsDropdown { get; set; }

        //[FindsBy(How = How.Id, Using = "373480134_2479050988")]
        public IWebElement WebDriverAdvancedDropdown { get; set; }

        //[FindsBy(How = How.Id, Using = "373480134_2479050998")]
        public IWebElement PageObjectDropdown { get; set; }

        //[FindsBy(How = How.Id, Using = "373480134_2479050999")]
        public IWebElement DataDrivenDropdown { get; set; }

        //[FindsBy(How = How.Id, Using = "373480134_2479051000")]
        public IWebElement SpecflowDropdown { get; set; }

        //[FindsBy(How = How.TagName, Using = "button")]
        public IWebElement NextPageButton { get; set; }
        #endregion

        public void SelectOptimalTeachersAnswer(int option)
        {
            RadioButtons[option - 1].Click();
        }

        public void SelectPracticeTimeAnswer(int option)
        {
            RadioButtons[3 + option].Click();
        }

        public void SelectInterestDropdownChoice(InterestDropdownOptions choice)
        {
            InterestDropdown.Click();
            new SelectElement(InterestDropdown).SelectByIndex((int)choice);
        }

        public void SelectTopicsRatings(RatingOptions webDriverChoice, RatingOptions webDriverAdvancedChoice,
                                        RatingOptions pageObjectChoice, RatingOptions dataDrivenTestingChoice, RatingOptions specflowChoice)
        {

            WebDriverBasicsDropdown.Click();
            new SelectElement(WebDriverBasicsDropdown).SelectByIndex((int)webDriverChoice);
            WebDriverAdvancedDropdown.Click();
            new SelectElement(WebDriverAdvancedDropdown).SelectByIndex((int)webDriverAdvancedChoice);
            PageObjectDropdown.Click();
            new SelectElement(PageObjectDropdown).SelectByIndex((int)pageObjectChoice);
            DataDrivenDropdown.Click();
            new SelectElement(DataDrivenDropdown).SelectByIndex((int)dataDrivenTestingChoice);
            SpecflowDropdown.Click();
            new SelectElement(SpecflowDropdown).SelectByIndex((int)specflowChoice);
        }

        public void ClickNextPage()
        {
            NextPageButton.Click();
        }
    }

    public enum InterestDropdownOptions
    {
        Empty = 0,
        WebdriverBasics = 1,
        WebdriverAdvanced = 2,
        PageObject = 3,
        DataDrivenTesting = 4,
        Specflow = 5
    }

    public enum RatingOptions
    {
        Empty = 0,
        One = 1,
        Two = 2,
        Three = 3,
        Four = 4,
        Five = 5
    }
}
