﻿namespace WebDriverBeadando_L91C6E.Pages.Widgets
{
    using OpenQA.Selenium;

    public class ResultWidget : BasePage
    {
        public ResultWidget(IWebDriver webDriver)
            : base(webDriver)
        {
        }

        public string GetTitle()
        {
            return Driver.Title;
        }
    }
}
