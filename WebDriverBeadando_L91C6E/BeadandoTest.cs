﻿using System;
using System.Collections;
using System.Collections.ObjectModel;
using System.Linq;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using WebDriverBeadando_L91C6E.Pages;
using WebDriverBeadando_L91C6E.Pages.Widgets;

namespace WebDriverBeadando_L91C6E
{
    public class BeadandoTest : TestBase
    {

        static IEnumerable testData()
        {
            return new string[]{ "2", "3"};
        }


        [Test, TestCaseSource("testData")]
        public void SurveyUITestMethod(string firstRadioButtonIndex)
        {
            FormPage formPage = new FormPage(Driver);
            formPage.Navigate();

            FormPartOneWidget formWidgetPartOne = formPage.GetFormWidgetPartOne();

            formWidgetPartOne.SelectOptimalTeachersAnswer(int.Parse(firstRadioButtonIndex));

            formWidgetPartOne.SelectPracticeTimeAnswer(3);

            formWidgetPartOne.SelectInterestDropdownChoice(InterestDropdownOptions.WebdriverBasics);

            formWidgetPartOne.SelectTopicsRatings(RatingOptions.Three, RatingOptions.One, RatingOptions.Two, RatingOptions.Four, RatingOptions.Five);

            formWidgetPartOne.ClickNextPage();

            FormPartTwoWidget formWidgetPartTwo = formPage.GetFormWidgetPartTwo();

            formWidgetPartTwo.SelectInterestDropdownChoice(3);

            formWidgetPartTwo.FillTextField("A gyakori beadandók egy kicsit szokatlanok, de egyébként nagyon érdekes és hasznos volt a tárgy.");

            formWidgetPartTwo.ClickSubmitButton();

            ResultWidget resultWidget = formPage.GetResultWidget();

            Assert.That(resultWidget.GetTitle().Contains("Thank you! Create Your Own Online Survey Now!"));
        }
    }
}
